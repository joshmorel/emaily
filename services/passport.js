const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const mongoose = require('mongoose');
mongoose.Promise = global.Promise; //mpromise deprecated
const keys = require('../config/keys');

const User = mongoose.model('users');

passport.serializeUser((user, done) => {
  //internal id --> mongodb.users._id
  //NOT google.profile.id
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  User.findById(id)
    .then(user => done(null, user));
});

passport.use(
  new GoogleStrategy({
      clientID: keys.googleClientID,
      clientSecret: keys.googleClientSecret,
      callbackURL: '/auth/google/callback',
      proxy: true
    },
    async (accessToken, refreshToken, profile, done) => {
      try {
        const existingUser = await User.findOne({googleId: profile.id});
        if (existingUser) {
          return done(null, existingUser);
        }
        const user = await User({googleId: profile.id}).save();
        done(null, user);
      } catch (err) {
        console.log(err);
      }
    }
  )
);
