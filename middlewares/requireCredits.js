module.exports = (req, res, next) => {
  //next is function we call when middleware is complete
  if (req.user.credits < 1) {
    return res.status(403).send({error: "Not enough credits!"});
  }

  next();
};
