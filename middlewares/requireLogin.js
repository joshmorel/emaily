module.exports = (req, res, next) => {
  //next is function we call when middleware is complete
  if (!req.user) {
    return res.status(401).send({error: 'You must log in!'});
  }

  next();
};
