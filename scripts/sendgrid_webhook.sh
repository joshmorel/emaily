#@IgnoreInspection BashAddShebang
# automatically restarts localtunnel if it crashes
# if using add to package.json "webhook": "./scripts/sendgrid_webhook.sh"
function localtunnel {
  lt -s emailydev19839189 --port 5000
}
until localtunnel; do
echo "localtunnel server crashed"
sleep 2
done
