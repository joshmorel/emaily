# Emaily

Learning Full Stack React with Express and MongoDB

## Resources

* [Official Course Repo](https://github.com/StephenGrider/FullstackReactCode)
* [Prettier](https://github.com/prettier/prettier)
* [Blog Posts on React](https://rallycoding.com/)
* [Author's Twitter](https://twitter.com/ste_grider)
* [Diagrams](https://github.com/StephenGrider/FullstackReactCode/tree/master/diagrams)

## Expansion Ideas

* [ ] Frontend - Easy - Improve design of survey list cards
* [ ] Backend - Easy - Allow users to delete surveys that have been created
* [ ] Front+Back - Medium - Allow users to specify the 'from' field on survey emails
* [ ] Front - Medium - Allow client side sorting of surveys
* [ ] Front+Back - Very Hard - Allow surveys to be created in 'draft mode'.
