const mongoose = require('mongoose');
const Path = require('path-parser');
const {URL} = require('url');
const requireLogin = require('../middlewares/requireLogin');
const requireCredits = require('../middlewares/requireCredits');
const Mailer = require('../services/Mailer');

const Survey = mongoose.model('surveys');
const surveyTemplate = require('../services/emailTemplates/surveyTemplate');

module.exports = app => {

  app.post('/api/surveys/webhooks', async (req, res) => {
    try {
      const p = new Path('/api/surveys/:surveyId/:choice');

      req.body.map(({email, url}) => {
        if (!email || !url) {
          return;
        }
        const match = p.test(new URL(url).pathname); //object or null
        if (match) {
          return {email, surveyId: match.surveyId, choice: match.choice};
        }
      })
        .filter(event => event) //only return defined & non-empty
        .reduce((uniq, nextEvent) => {
          // take only last event for each email/surveyId combo
          return uniq.filter(event => {
            return !(event.email === nextEvent.email && event.surveyId === nextEvent.surveyId)
          }).concat(nextEvent)
        }, [])
        .forEach(({email, surveyId, choice}) => {
          Survey.updateOne({
            _id: surveyId,
            recipients: {
              $elemMatch: {email, responded: false}
            }
          }, {
            $inc: {[choice]: 1},
            $set: {'recipients.$.responded': true},
            lastResponded: new Date()
          }).exec();
        });
    }
    catch (err) {
      console.error(err);
    } finally {
      res.send({});
    }
  });

  app.get('/api/surveys/:surveyId/:answer', (req, res) => {
    res.send(`Thanks for voting! You voted ${req.params.answer}`);
  });

  app.get('/api/surveys', requireLogin, async (req, res) => {
    const surveys = await Survey.find({_user: req.user.id})
      .select({recipients: false});

    res.send(surveys);
  });

  app.post('/api/surveys', requireLogin, requireCredits, async (req, res) => {
      const {title, subject, body, recipients} = req.body;

      const survey = new Survey({
        title,
        subject,
        body,
        recipients: recipients.split(',').map(email => ({email: email.trim()})),
        _user: req.user.id,
        dateSent: Date.now()
      });

      // Great place to send an email!
      const mailer = new Mailer(survey, surveyTemplate(survey));
      try {
        await mailer.send();
        await survey.save();
        req.user.credits -= 1;
        const user = await req.user.save();
        res.send(user);
      } catch (err) {
        res.status(422).send(err);
      }
    }
  );
};
