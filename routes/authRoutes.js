const passport = require('passport');

module.exports = (app) => {
  app.get(
    '/auth/google',
    passport.authenticate('google', {
      //list of scopes: https://developers.google.com/identity/protocols/googlescopes
      scope: ['profile', 'email']
    })
  );
  app.get(
    //callback returns ?code=verylongcode, so passport.authenticate('google') will handle differently
    '/auth/google/callback',
    passport.authenticate('google'),
    //passport is middleware, so can continue with req/res function
    (req, res) => {
      res.redirect('/surveys');
    }
  );
  app.get('/auth/fail', (req, res) => {
    res.send("Authentication Failed ¯\\_(⊙︿⊙)_/¯")
  });
  app.get('/api/logout', (req, res) => {
    req.logout();
    res.redirect('/');
  });
  app.get('/api/current_user', (req, res) => {
    res.send(req.user);
  });
};

